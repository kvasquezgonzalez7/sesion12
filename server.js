const express = require('express');
const app = express();
const env = require("./env.environment.json");
const puerto = env.development.PORT;
app.use(express.urlencoded({extendent:true}))
app.use(express.json());

var autores = [
    {
    id:1,
    nom: "Jorgue Luis",
    ape:"Borges",
    fdn: "24/08/1899",
    libros:[
        {
            id:1,
            titulo:"Ficciones",
            descripcion:"se trata de uno de sus mas...",
            aniopublicacion:1944
        },
        {
            id:2,
            titulo:"El aleph",
            description: "otra recoplicacion de cuentos...",
            aniopublicacion:1949
        }
    ],
  },
];


//MIDDLEWARE
function verificarAutor(req,res,next){
    let id = req.params.id;
    let resp = autores.filter((item) => item.id === parseInt(id));
    if(resp.length === 0){
        res.json("NO EXISTE EL AUTOR")
    }else{
        next();
    }
}

function verificarLibro(req,res,next){
    let id = parseInt(req.params.id);
    let libro = parseInt(req.params.libro);
    autores.forEach(element => {
        if(element.id === id){
            resp = element.libros.filter(x => x.id === libro);

        }
    });
    if(resp.length === 0){
        res.json("NO EXISTE EL LIBRO")
    }else{
        next();
    }
}

//Listar autorees

app.get("/autores", function(req,res){
    res.json(autores);
});

// Agregar AUTOR

app.post("/autores", function(req,res){

if(!req.body.id || !req.body.nom || !req.body.ape || !req.body.fdn){
    res.json("SE REQUIEREN LOS CAMPOS NOMBRE,APELLIDO Y FECHA DE NACIMIENTO");
}else{
    if (req.body.id !=="" || req.body.nom !=="" || req.body.ape !=="" || req.body.fdn !==""){
        datos = {
            id: req.body.id,
            nom: req.body.nom,
            ape: req.body.ape,
            fdn: req.body.fdn,
            libros:[]
        };
       autores.push(datos);
       res.json("AUTOR REGISTRADO");

    }
}

});


// TRAER UN AUTOR-----
app.get("/autores/:id", verificarAutor, function(req,res){
    res.json(autores.filter((item) => item.id === parseInt(req.params.id)));
});

//ELIMINAR AUTOR------
app.delete("/autores/:id", verificarAutor, function(req,res){
    if(!req.params.id){
        res.json("SE REQUIEREN PONER EL ID");
    }else{
        if(req.params.id !==""){
            let filtro = autores.filter((x) => x.id !== parseInt(req.params.id));
            autores = filtro;
            res.json("AUTOR ELIMINADO");
        }
    }
});

//ACTUALIZAR AUTOR------
app.put("/autores/:id", verificarAutor, function(req, res){
    if(!req.body.nom || !req.body.ape || !req.body.fdn){
        res.json("SE REQUIEREN LOS CAMPOS NOMBRE, APELLIDO Y FECHA DE NACIMIENTO");
    }else{
        if(req.body.nom !=="" || req.body.ape !=="" || req.body.fdn !==""){

            autores.map((item) => {
                if (item.id === parseInt(req.params.id)){
                    item.id = req.params.id,
                    item.nom = req.body.nom,
                    item.ape = req.body.ape,
                    item.fdn = req.body.fdn,

                    res.json("AUTOR ACTUALIZADO");

                }

            });
        }
    }
});


//LISTAR TODOS LOS LIBROS DE UN AUTOR

app.get("/autores/:id/libros",verificarAutor,(req,res) => {
    let i;
    autores.forEach((item,index)=>{
        if(item.id === parseInt(req.params.id))i=index;

    });

    res.json(autores[i].libros);

});

//AGREGAR LIBRO DE UN ACTOR
app.post("/autores/:id/libros",verificarAutor,(req,res)=>{
   let i;
   autores.forEach((item,index)=>{
       if(item.id===parseInt(req.params.id))i=index;
   });

   if (!req.body.id || !req.body.titulo || !req.body.descripcion || !req.body.aniopublicacion) {
       res.json("SE REQUIEREN LOS CAMPOS ID,TITULO,DESCRIPCION Y AÑO DE PUBLICACION");
       
    }else{
        if (req.body.id !== "" || req.body.titulo !== "" || req.body.descripcion !== "" || req.body.aniopublicacion !== "") {

        datos = {
           id: req.body.id,
           titulo: req.body.titulo,
           descripcion: req.body.descripcion,
           aniopublicacion: req.body.aniopublicacion
       };

    autores[i].libros.push(datos);
    res.json("LIBRO REGISTRADO");

   }

}

});

//Listar un libro de un autor
app.get("/autores/:id/libros/:libro",verificarAutor,verificarLibro,(req,res)=>{
    let id = parseInt(req.params.id);
    let libro = parseInt(req.params.libro);

    autores.forEach(item=>{
        if(item.id === id){
            item.libros.forEach(item => {
                if(item.id === libro)res.json(item);
            })
            
        }
    })

    res.json("NO SE ENCONTRO EL LIBRO");
     
});

//ELIMINAR UN LIBRO DE UN AUTOR

app.delete("/autores/:id/libros/:libro", verificarAutor,verificarLibro,(req,res)=>{
    let datos;
    let id = parseInt(req.params.id);
    let idlibro = parseInt(req.params.libro);
    
    autores.forEach((item,index)=>{
        if(item.id===id){
            datos = item.libros.filter(x => x.id !== idlibro);
            i = index;
        }
    });
    autores[i].libros = datos;

    res.json("LIBRO ELIMINADO");

});

//ACTUALIZAR UN LIBRO DE UN AUTOR

app.put("/autores/:id/libros/:libro",verificarAutor,verificarLibro,(req,res)=>{
    let datos;
    let id = parseInt(req.params.id);
    let idlibro = parseInt(req.params.libro);
    if(!req.body.titulo || !req.body.descripcion || !req.body.aniopublicacion ){
        res.json("SE REQUIERE EL CAMPO TITULO,DESCRIPCION Y EL AÑO DE PUBLICACION");
    }else{
        if(req.body.titulo !=="" || req.body.descripcion !=="" || req.body.aniopublicacion !==""){
            autores.forEach((item,index)=>{
              if(item.id===id){
                  item.libros.forEach((item,index1)=>{
                      if(item.id === idlibro){
                          datos = {
                              id: idlibro,
                              titulo: req.body.titulo,
                              descripcion: req.body.descripcion,
                              aniopublicacion: req.body.aniopublicacion
                          }
                          i=index;
                          j=index1;
                      }
                  });
              }
            });
            autores[i].libros[j]=datos;
            res.json("LIBRO ACTUALIZADO");
        }
    }

})

app.listen(puerto, () => console.log("escuchando puerto 5000"));